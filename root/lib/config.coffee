config = require '../config'

config.production ?= {}
config.staging ?= {}
config.development ?= {}
config.env = config[process.env.NODE_ENV or 'development']

module.exports = config
