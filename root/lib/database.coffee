config      = require './config'
knex        = require 'knex'
bookshelf   = require 'bookshelf'

connection = knex config.env.database
bookshelf = bookshelf connection

bookshelf.plugin 'registry'
bookshelf.plugin 'virtuals'

module.exports.connection = connection
module.exports.bookshelf = bookshelf
