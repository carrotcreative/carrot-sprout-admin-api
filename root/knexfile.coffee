config = require './lib/config'

module.exports =
  development: config.development.database
  staging: config.staging.database
  production: config.production.database
