module.exports =
  production:
    database:
      client: 'pg'
      connection: process.env.DATABASE_URL

  development:
    database:
      client: 'mysql'
      connection:
        host: 'localhost'
        user: 'root'
        password: ''
        database: '<%= name %>_development'
        timezone: 'EST'
      migrations:
        tableName: 'migrations'
