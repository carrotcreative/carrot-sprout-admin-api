path  = require 'path'
fs    = require 'fs'
uuid  = require 'node-uuid'

token_path = path.join __dirname, '..', '..', 'token'
fs.writeFileSync token_path, uuid.v1()
