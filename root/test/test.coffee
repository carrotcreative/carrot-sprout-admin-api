app     = require '../lib'
chai    = require 'chai'
http    = require 'http'

should  = chai.should()

options =
  host: 'localhost'
  port: 4567

describe 'API', ->

  server = null

  before (done) ->
    server = app.listen options.port, ->
      done()

  after (done) ->
    server.close()
    done()

  it 'should exist', (done) ->
    should.exist app
    done()

  it "should be listening at localhost:#{options.port}", (done) ->
    http.get host: options.host, port: options.port, method: 'GET', (res) ->
      res.statusCode.should.eql 404
      done()
