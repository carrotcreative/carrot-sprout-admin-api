# sprout-admin-api

A sprout template for creating sprout express apis

### Installation

- `npm install sprout -g`
- `sprout add admin-api https://github.com/carrot/sprout-admin-api.git`

### Options

- **name** (name of template)
- **github_username** (name of github user)
- **description** (a short description of the template)
